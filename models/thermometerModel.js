
const mongoose = require('mongoose');

const thermometerSchema = new mongoose.Schema({
  datetime: {
    type: Date,
    required: true,
  },
  temperature: {
    type: Number,
    required: true,
  },
});

const Thermometerrecord = mongoose.model('thermometer_data', thermometerSchema);



module.exports = {Thermometerrecord};

