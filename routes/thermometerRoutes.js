// Routes File
const express = require('express');
const router = express.Router();
const cors = require('cors');
const thermometerController = require('../controllers/thermometerController.js');
router.options('/api/thermometer/upload', cors());
const multer = require('multer');
const upload = multer();


router.post('/api/thermometer/upload',upload.single('thermFile'), thermometerController.saveThermometerData, async (req, res) => {
    try {
      console.log("File uploading...");
    } catch (error) {
      console.error('Error handling file upload:', error.message);
      res.status(500).json({ error: 'Internal server error' });
    }
  });

router.get('/api/thermometer/history', async (req, res) => {
  try {
    await thermometerController.getTemperatureHistory(req, res);
  } catch (error) {
    console.error('Error handling temperature history request:', error.message);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;


