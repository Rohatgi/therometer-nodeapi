//Starting Page index file
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors'); 
const thermometerRoutes = require('./routes/thermometerRoutes');
const connectDB = require('./db'); 
const app = express();
const port = process.env.PORT || 3001;
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json'); 


// Middleware for parsing JSON requests
app.use(bodyParser.json());

// Swagger setup
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Import your routes and other middleware here
app.use(express.json());
app.use('/uploads', express.static('uploads'));

// Enable CORS for a specific origin

app.use(cors());

// Routes

app.use(thermometerRoutes);

// Connect to MongoDB
connectDB();


// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
