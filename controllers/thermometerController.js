
const ThermometerModel = require('../models/thermometerModel');


const saveThermometerData = async (req, res) => {
  try {
    const fileBuffer = req.file.buffer.toString();
    const thermometerData = JSON.parse(fileBuffer);
    //deleting previous data
    await ThermometerModel.Thermometerrecord.deleteMany({});
    // Assuming the data structure is an array of objects with 'ts' and 'val' properties
    const chunkSize = 1000; // You can adjust the chunk size based on your needs
    for (let i = 0; i < thermometerData.length; i += chunkSize) {
      const chunk = thermometerData.slice(i, i + chunkSize);
     
      // Convert 'ts' to 'timestamp' and 'val' to 'temperature' before saving
      const formattedData = chunk.map(entry => ({
        datetime:entry.ts, 
        temperature: entry.val,
      }));
     
      await ThermometerModel.Thermometerrecord.insertMany(formattedData);
      res.status(200).json({ message: 'Thermometer data saved successfully.' });
        
      
    }

  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// thermometerController.js

const getTemperatureHistory = async (req, res) => {
  try {
    // Aggregation pipeline to select a single record for each unique datetime
    const temperatureHistory = await ThermometerModel.Thermometerrecord.aggregate([
      {
        $group: {
          _id: {
            datetime: '$datetime',
          },
          record: { $first: '$$ROOT' },
        },
      },
      {
        $project: {
          _id: 0,
          datetime: '$record.datetime',
          temperature: '$record.temperature',
        },
      },
      {
        $sort: { datetime: 1 },
      },
    ]);

    res.status(200).json(temperatureHistory);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


module.exports = { saveThermometerData, getTemperatureHistory };

