### Thermometer Node Express API project

### MongoDb ###
1. install MongoDb server and mongodb compass locally
2. Connect using this -> mongodb+srv://rohatgiamrita087:AEC9RNDOrTINKUlu@cluster0.lkgnuwn.mongodb.net/therometer in compass 

### For Nodejs Express##
1. Install latest version of node from https://nodejs.org/en
2. Install git for using git commands (for cloning the project) [userGuide- https://github.com/git-guides/install-git]
3. using git clone <<git_url which has shared>
4. Install npm install (all dependencies are mention in package.json file )
5. Now make sure nothing is running at port 3001
6. Finally run node index or npm start (To run Node JS API locally )

### Swagger API Documentation###
1.Install swagger using command npm install express swagger-ui-express
2.Visit http://localhost:3000/api-docs to access the Swagger UI and explore your API documentation.
Note- Make sure nodejs project is running locally

### JEST Test Case ###
1. Install JEST using command npm install --save-dev jest supertest
2. Command to run testcases in terminal -> npm run test
3. Added basic test cases for my APIs and for db connection . Make changes to run those test cases. I check and run successfully.

EditorUsed- VisualStudio
