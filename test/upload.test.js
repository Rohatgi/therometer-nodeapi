const request = require('supertest');
const app = require('../index.js'); 
//upload test
describe('File Upload API Tests', () => {
  test('should upload a file successfully', async () => {
    const response = await request(app)
      .post('/api/thermometer/upload')
      .attach('thermFile', 'path/to/your/test/filename.json'); // Provide a path to a test JSON file

    expect(response.status).toBe(201);
    expect(response.body.message).toBe('Data uploaded successfully');
  });

  test('should handle file upload error', async () => {
    const response = await request(app)
      .post('/api/thermometer/upload')
      .attach('thermFile', 'path/to/nonexistent/file.json'); // Provide a path to a nonexistent file

    expect(response.status).toBe(500);
    expect(response.body.error).toBe('Internal Server Error');
  });
});
