const mongoose = require('mongoose');
const connectDB = require('../db.js');

describe('Test MongoDB Connection', () => {
  // Mock mongoose.connect function
  jest.mock('mongoose');
  mongoose.connect = jest.fn();

  it('should connect to MongoDB successfully', async () => {
    await connectDB();
    expect(mongoose.connect).toHaveBeenCalled();
  });
});
